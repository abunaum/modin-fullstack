# Modin Fullstack

![](https://img.shields.io/github/license/abunaum/modin-nodejs) ![](https://img.shields.io/badge/-Nodejs-success) ![](https://img.shields.io/badge/-Express-blue) ![](https://img.shields.io/badge/-React-blue) ![](https://img.shields.io/static/v1?label=DB&message=Mongo&color=ffff)

## Installation
Modin membutuhkan [Node.js](https://nodejs.org/) v16+ agar dapat berjalan.

Install dependencies dan jalankan server.

```sh
cd modin-fullstack/backend
npm intall
npm start
```

Install dependencies dan jalankan client.

```sh
cd modin-fullstack/dashboard-v1
npm intall
npm start
```

## Akses Panel

Untuk mengakses panel pastikan server dan client sudah di jalankan, lalu buka browser dan akses dengan port 3000

```sh
localhost:3000
```

## License

***MIT***
