import React, { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import Footer from "../../../components/Footer";
import Navbar from "../../../components/Navbar";
const NikahMasuk = ({ data }) => {
  const [nikahmasuk, setnikahmasuk] = useState(null);
  useEffect(() => {
    const getNikahmasuk = () => {
      fetch("http://localhost:5000/nikah/masuk", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          throw new Error("Autentikasi Gagal");
        })
        .then((result) => {
          setnikahmasuk(result);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getNikahmasuk();
  }, []);
  const columns = [
    {
      name: "Nomor Register",
      selector: (row) => row.noreg,
    },
    {
      name: "Tempat Nikah",
      selector: (row) => row.acara.tempat_nikah,
    },
  ];
  const ExpandedComponent = ({ nikahmasuk }) => (
    <pre>{JSON.stringify(nikahmasuk, null, 2)}</pre>
  );
  console.log(nikahmasuk);
  return (
    <main className="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
      <Navbar datanav={data} />
      <div className="container-fluid py-4">
        <center>
          <h1>Halaman Nikah Masuk</h1>
        </center>
        {nikahmasuk && (
          <DataTable
            columns={columns}
            data={nikahmasuk}
            selectableRows
            expandableRowsComponent={ExpandedComponent}
            pagination
          />
        )}
        <Footer />
      </div>
    </main>
  );
};

export default NikahMasuk;
