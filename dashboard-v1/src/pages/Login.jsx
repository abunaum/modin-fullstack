import React, { Component } from "react";

export class Login extends Component {
  render() {
    const google = () => {
      window.open("http://localhost:5000/auth/google", "_self");
    };
    return (
      <div className="wrapper">
        <div className="logo">
          <img
            src="https://avatars.githubusercontent.com/u/70275608?v=4"
            alt=""
          />
        </div>
        <div className="mt-4 center-text">Silahkan login terlebih dahulu</div>
        <br />
        <div className="p-3">
          <center>
            <div className="btn-google" onClick={google}>
              Login With Google
            </div>
          </center>
        </div>
        <div className="p-3">
          <center>
            <div className="btn-github">Login With Github</div>
          </center>
        </div>
      </div>
    );
  }
}

export default Login;
