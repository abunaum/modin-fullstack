import Footer from "../components/Footer";
import Navbar from "../components/Navbar";

const Dashboard = ({ data }) => {
  return (
    <>
      <main className="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <Navbar datanav={data} />
        <div className="container-fluid py-4">
          <center>
            <h1>Halaman Beranda</h1>
          </center>
          <Footer />
        </div>
      </main>
    </>
  );
};

export default Dashboard;

// async function getUser() {
//   var data = [];
//   var setUser = [];
//   await fetch("http://localhost:5000/auth/login/success", {
//     method: "GET",
//     credentials: "include",
//     headers: {
//       Accept: "application/json",
//       "Content-Type": "application/json",
//       "Access-Control-Allow-Credentials": true,
//     },
//   })
//     .then((response) => {
//       if (response.status !== 200) {
//         Object.assign(data, response.json());
//       } else {
//         throw new Error("Autentikasi gagal");
//       }
//     })
//     .then(() => {
//       Object.assign(setUser, data.user);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
//   return setUser;
// }
