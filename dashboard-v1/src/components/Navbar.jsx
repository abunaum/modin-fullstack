import React from "react";

const Navbar = ({ datanav }) => {
  const ceksidebar = () => {
    if (document.body.classList.contains("g-sidenav-pinned")) {
      document.body.classList.add(
        "bg-salmon",
        "my-class-1",
        "my-class-2",
        "my-class-3"
      );
    } else {
      document.body.classList.remove(
        "bg-salmon",
        "my-class-1",
        "my-class-2",
        "my-class-3"
      );
    }
    console.log(document.body.classList);
  };
  const logout = () => {
    window.open("http://localhost:5000/auth/logout", "_self");
  };
  return (
    <nav
      className="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl"
      id="navbarBlur"
      data-scroll="true"
    >
      <div className="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <h6 className="font-weight-bolder mb-0">{datanav.halaman}</h6>
        </nav>
        <div
          className="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4"
          id="navbar"
        >
          <div className="ms-md-auto pe-md-3 d-flex align-items-center"></div>
          <ul className="navbar-nav  justify-content-end">
            <li className="nav-item d-flex align-items-center">
              <div className="nav-link text-body font-weight-bold px-0 dropdown">
                <a
                  href="/"
                  className="d-sm-inline d-none dropdown-toggle"
                  data-bs-toggle="dropdown"
                >
                  {datanav.user.emails[0].value}
                </a>
                <div className="dropdown-menu">
                  <p className="dropdown-item" onClick={logout}>
                    Logout
                  </p>
                </div>
              </div>
            </li>
            <li className="nav-item d-xl-none ps-3 d-flex align-items-center">
              <div
                className="nav-link text-body p-0"
                id="iconNavbarSidenav"
                onClick={ceksidebar}
              >
                <div className="sidenav-toggler-inner">
                  <i className="sidenav-toggler-line"></i>
                  <i className="sidenav-toggler-line"></i>
                  <i className="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
