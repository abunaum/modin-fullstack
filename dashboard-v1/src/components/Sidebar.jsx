import React from "react";
import { Link } from "react-router-dom";

const Sidebar = (props) => {
  function cekaktiv(bar) {
    if (props.halaman === bar) {
      return "nav-link text-dark active bg-gradient-success";
    } else {
      return "nav-link text-dark";
    }
  }

  const closesidebar = () => {
    document.body.className = "g-sidenav-show bg-gray-200";
  };
  return (
    <aside
      className="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-white"
      id="sidenav-main"
      onClick={closesidebar}
    >
      <div className="sidenav-header">
        <i
          className="fas fa-times p-3 cursor-pointer opacity-5 position-absolute end-0 top-0 d-xl-none text-dark"
          aria-hidden="true"
          id="iconSidenav"
        ></i>
        <Link className="navbar-brand m-0" to="/">
          <img
            src="https://avatars.githubusercontent.com/u/70275608?v=4"
            className="navbar-brand-img h-100"
            alt="main_logo"
          />
          <span className="ms-1 font-weight-bold text-dark">Modin</span>
        </Link>
      </div>
      <hr className="horizontal light mt-0 mb-2" />
      <div className="sidebar-wrapper">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className={cekaktiv("Beranda")} to="/">
              <div className="text-dark text-center me-2 d-flex align-items-center justify-content-center">
                <span
                  className="iconify"
                  data-icon="clarity:home-line"
                  data-width="32"
                  data-height="32"
                ></span>
              </div>
              <span className="nav-link-text ms-1">Beranda</span>
            </Link>
          </li>
          <li className="nav-item mt-3">
            <h6 className="ps-4 ms-2 text-uppercase text-xs text-dark font-weight-bolder opacity-8">
              Nikah
            </h6>
          </li>
          <li className="nav-item">
            <Link
              className={cekaktiv("Nikah Masuk")}
              to={{
                pathname: "/nikah/masuk",
                state: { halaman: "Nikah Masuk" },
              }}
            >
              <div className="text-dark text-center me-2 d-flex align-items-center justify-content-center">
                <span
                  className="iconify"
                  data-icon="codicon:sign-in"
                  data-width="32"
                  data-height="32"
                ></span>
              </div>
              <span className="nav-link-text ms-1">Masuk</span>
            </Link>
            <a className="nav-link text-dark " href="/nikah/keluar">
              <div className="text-dark text-center me-2 d-flex align-items-center justify-content-center">
                <span
                  className="iconify"
                  data-icon="codicon:sign-out"
                  data-width="32"
                  data-height="32"
                ></span>
              </div>
              <span className="nav-link-text ms-1">Keluar</span>
            </a>
            <a className="nav-link text-dark " href="/nikah/semua-data">
              <div className="text-dark text-center me-2 d-flex align-items-center justify-content-center">
                <span
                  className="iconify"
                  data-icon="uiw:reload"
                  data-width="32"
                  data-height="32"
                ></span>
              </div>
              <span className="nav-link-text ms-1">Semua Data</span>
            </a>
          </li>
          <li className="nav-item mt-3">
            <h6 className="ps-4 ms-2 text-uppercase text-xs text-dark font-weight-bolder opacity-8">
              Person
            </h6>
          </li>
          <li className="nav-item">
            <a className="nav-link text-dark " href="/person">
              <div className="text-dark text-center me-2 d-flex align-items-center justify-content-center">
                <span
                  className="iconify"
                  data-icon="ph:users-three-thin"
                  data-width="32"
                  data-height="32"
                ></span>
              </div>
              <span className="nav-link-text ms-1">Data Person</span>
            </a>
            <a className="nav-link text-dark" href="../pages/profile.html">
              <div className="text-dark text-center me-2 d-flex align-items-center justify-content-center">
                <span
                  className="iconify"
                  data-icon="fluent:people-search-20-regular"
                  data-width="32"
                  data-height="32"
                ></span>
              </div>
              <span className="nav-link-text ms-1">Cek Person</span>
            </a>
          </li>
          <li className="nav-item mt-3">
            <h6 className="ps-4 ms-2 text-uppercase text-xs text-dark font-weight-bolder opacity-8">
              Setting
            </h6>
          </li>
          <li className="nav-item">
            <a className="nav-link text-dark " href="/setting">
              <div className="text-dark text-center me-2 d-flex align-items-center justify-content-center">
                <span
                  className="iconify"
                  data-icon="ep:setting"
                  data-width="32"
                  data-height="32"
                ></span>
              </div>
              <span className="nav-link-text ms-1">Setting Aplikasi</span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default Sidebar;
