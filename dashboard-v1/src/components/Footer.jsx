import React from "react";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <footer className="footer py-4  ">
      <div className="container-fluid">
        <div className="row align-items-center justify-content-lg-between">
          <div className="mb-lg-0 mb-4">
            <div className="copyright text-center text-sm text-muted text-lg-start">
              © {new Date().getFullYear()}, made with{" "}
              <i className="fa fa-heart"></i> by &nbsp;
              <Link
                to="https://www.facebook.com/ahmad.yani.ardath"
                className="font-weight-bold"
                target="_blank"
              >
                Abunaum
              </Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
