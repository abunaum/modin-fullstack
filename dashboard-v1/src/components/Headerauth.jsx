import React from "react";
import { HelmetProvider, Helmet } from "react-helmet-async";

const Headerauth = (props) => {
  return (
    <HelmetProvider>
      <Helmet>
        <meta charset="UTF-8" />
        <meta
          name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
        />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link
          rel="icon"
          type="image/png"
          href="https://avatars.githubusercontent.com/u/70275608?v=4"
        />
        <title>{props.halaman}</title>
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx"
          crossorigin="anonymous"
        />
        <link
          href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          rel="stylesheet"
        />
        <link href="/assets/css/auth.css" rel="stylesheet" />
      </Helmet>
    </HelmetProvider>
  );
};

export default Headerauth;
