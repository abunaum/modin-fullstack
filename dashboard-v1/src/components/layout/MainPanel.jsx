import React from "react";
import { Outlet } from "react-router-dom";
// Components
import Headerpanel from "../HeaderPanel";
import Sidebar from "../Sidebar";

const MainPanel = (data) => {
  return (
    <>
      <Headerpanel halaman={data.halaman} />
      <Sidebar halaman={data.halaman} />
      <Outlet />
    </>
  );
};

export default MainPanel;
