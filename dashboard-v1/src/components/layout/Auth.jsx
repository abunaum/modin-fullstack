import React from "react";
import { Outlet } from "react-router-dom";
import Headerauth from "../Headerauth";

const Auth = (data) => {
  return (
    <>
      <Headerauth halaman={data.halaman} />
      <Outlet />
    </>
  );
};

export default Auth;
