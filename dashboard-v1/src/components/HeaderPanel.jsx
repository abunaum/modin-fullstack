import React from "react";
import { HelmetProvider, Helmet } from "react-helmet-async";

const HeaderPanel = (props) => {
  return (
    <HelmetProvider>
      <Helmet>
        <meta charset="utf-8" />
        <link
          rel="icon"
          href="https://avatars.githubusercontent.com/u/70275608?v=4"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta
          name="description"
          content="Web site created using create-react-app"
        />
        <link rel="apple-touch-icon" href="/logo192.png" />
        <link rel="manifest" href="/manifest.json" />
        <title>{props.halaman}</title>
        <link
          href="https://demos.creative-tim.com/material-dashboard/assets/css/nucleo-icons.css"
          rel="stylesheet"
        />
        <link
          href="https://demos.creative-tim.com/material-dashboard/assets/css/nucleo-svg.css"
          rel="stylesheet"
        />
        <script
          src="https://kit.fontawesome.com/42d5adcbca.js"
          crossorigin="anonymous"
        ></script>
        <link
          href="https://fonts.googleapis.com/icon?family=Material+Icons+Round"
          rel="stylesheet"
        />
        <link
          id="pagestyle"
          href="https://demos.creative-tim.com/material-dashboard/assets/css/material-dashboard.min.css?v=3.0.4"
          rel="stylesheet"
        />

        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script src="https://demos.creative-tim.com/material-dashboard/assets/js/core/popper.min.js"></script>
        <script src="https://demos.creative-tim.com/material-dashboard/assets/js/core/bootstrap.min.js"></script>
        <script src="https://demos.creative-tim.com/material-dashboard/assets/js/plugins/perfect-scrollbar.min.js"></script>
        <script src="https://demos.creative-tim.com/material-dashboard/assets/js/plugins/smooth-scrollbar.min.js"></script>
        <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
        <script src="https://code.iconify.design/iconify-icon/1.0.0-beta.3/iconify-icon.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <script src="https://demos.creative-tim.com/material-dashboard/assets/js/material-dashboard.min.js?v=3.0.4"></script>

        <script>
          {`
          var win = navigator.platform.indexOf("Win") > -1;
          if (win && document.querySelector("#sidenav-scrollbar")) {
            var options = {
              damping: "0.5",
            };
            Scrollbar.init(document.querySelector("#sidenav-scrollbar"), options);
          }
          `}
        </script>
      </Helmet>
    </HelmetProvider>
  );
};

export default HeaderPanel;
