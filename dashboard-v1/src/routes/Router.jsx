import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// Components
import MainPanel from "../components/layout/MainPanel";
import Auth from "../components/layout/Auth";
// Pages
import Dashboard from "../pages/Dashboard";
import Login from "../pages/Login";
import NikahMasuk from "../pages/Nikah/Masuk/NikahMasuk";
import Page404 from "../pages/Page404";

const Router = () => {
  const [user, setUser] = useState(null);
  useEffect(() => {
    const getUser = () => {
      fetch("http://localhost:5000/auth/login/success", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          throw new Error("Autentikasi Gagal");
        })
        .then((resObject) => {
          setUser(resObject.user);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getUser();
  }, []);
  if (user) {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/">
            <Route
              index
              element={
                <>
                  <MainPanel halaman="Beranda" />
                  <Dashboard data={{ user, halaman: "Beranda" }} />
                </>
              }
            />
            <Route path="nikah">
              <Route
                path="masuk"
                element={
                  <>
                    <MainPanel halaman="Nikah Masuk" />
                    <NikahMasuk data={{ user, halaman: "Nikah Masuk" }} />
                  </>
                }
              />
            </Route>
          </Route>
          <Route path="*" element={<Page404 />} />
        </Routes>
      </BrowserRouter>
    );
  } else {
    return (
      <BrowserRouter>
        <Routes>
          <Route
            path="*"
            element={
              <>
                <Auth halaman="Login" />
                <Login />
              </>
            }
          />
        </Routes>
      </BrowserRouter>
    );
  }
};

export default Router;
