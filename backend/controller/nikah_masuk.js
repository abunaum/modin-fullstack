const { NikahMasuk } = require("../model/nikah");

const view_masuk = async (req, res) => {
  var datanikah = await NikahMasuk.find({ status: "masuk" }).lean();
  await datanikah.sort(function (a, b) {
    return a.tglregister > b.tglregister;
  });
  res.send(datanikah);
};

module.exports = { view_masuk };
