const router = require("express").Router();
const passport = require("passport");
const client_url = "http://localhost:3000";

router.get("/login/success", (req, res) => {
  if (req.user && req.user.emails[0].value === "abunaum@hotmail.com") {
    res.status(200).json({
      success: true,
      message: "succes",
      user: req.user,
    });
  } else {
    res.redirect("logout");
  }
});

router.get("/logout", (req, res) => {
  req.logout();
  res.redirect(client_url);
});

router.get("/login/failed", (req, res) => {
  res.status(401).json({
    success: false,
    message: "failure",
  });
});

router.get(
  "/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);

router.get(
  "/google/callback",
  passport.authenticate("google", {
    successRedirect: client_url,
    failureFlash: "/auth/login/failed",
  })
);

module.exports = router;
