const Nikah = require("express").Router();
const express = require("express");
const {
  view_masuk,
  delete_masuk,
  tambah_masuk,
  edit_masuk,
  view_edit_masuk,
  view_tambah_masuk,
  detail_masuk,
} = require("../controller/nikah_masuk");
const {
  view_keluar,
  view_tambah_keluar,
  tambah_keluar,
  view_edit_keluar,
  edit_keluar,
  detail_keluar,
  delete_keluar,
} = require("../controller/nikah_keluar");
const { semuadata } = require("../controller/nikah_semua");

Nikah.get("/masuk", view_masuk);

module.exports = Nikah;
