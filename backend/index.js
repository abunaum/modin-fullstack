const express = require("express");
const cookieSession = require("cookie-session");
const passport = require("passport");
require("./passport");
require("./utils/db");
const cors = require("cors");
const app = express();
const port = 5000;

app.use(
  cookieSession({
    name: "session",
    keys: ["yaniardath!@#"],
    maxAge: 24 * 60 * 60 * 100,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(
  cors({
    origin: "http://localhost:3000",
    methods: "GET,POST,PUT,DELETE",
    credentials: true,
  })
);

const authRoutes = require("./routes/auth");
const nikahRoutes = require("./routes/nikah");
app.get("/", (req, res) => res.send("Hello World!"));
app.use("/auth", authRoutes);
app.use("/nikah", nikahRoutes);
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
